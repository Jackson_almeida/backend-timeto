import { Events } from '../events/events.entity';
import { User } from '../user/user.entity';
export declare enum InviteStatus {
    ACCEPTED = "accepted",
    REJECTED = "rejected",
    PENDING = "pending"
}
export declare class Invite {
    id: number;
    guestUser: User;
    event: Events;
    status: InviteStatus;
}
