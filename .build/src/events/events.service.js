"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventsService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_service_1 = require("../user/user.service");
const typeorm_2 = require("typeorm");
const events_entity_1 = require("./events.entity");
let EventsService = class EventsService {
    constructor(eventsRepository, userService) {
        this.eventsRepository = eventsRepository;
        this.userService = userService;
    }
    async getAll() {
        return this.eventsRepository.find();
    }
    async getById(id) {
        return this.eventsRepository.findOne(id);
    }
    async getAllByUser(id) {
        return this.eventsRepository.find({ where: { user: id } });
    }
    async createEvent(userId, createEventDto) {
        const event = new events_entity_1.Events();
        Object.assign(event, createEventDto);
        const user = await this.userService.getById(userId);
        event.user = user;
        return this.eventsRepository.save(event);
    }
    async editEvent(id, createEventDto) {
        const event = await this.eventsRepository.findOne(id);
        event.name = createEventDto.eventname;
        event.description = createEventDto.description;
        event.initialDatetime = createEventDto.initialDatetime;
        event.finalDatetime = createEventDto.finalDatetime;
        return this.eventsRepository.save(event);
    }
    async deleteEvent(id) {
        return this.eventsRepository.delete(id);
    }
};
EventsService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(events_entity_1.Events)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        user_service_1.UserService])
], EventsService);
exports.EventsService = EventsService;
//# sourceMappingURL=events.service.js.map