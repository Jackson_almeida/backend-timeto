import { Repository } from 'typeorm';
import { CreateUserDto } from '../dto/create-user.dto';
import { User } from './user.entity';
export declare class UserService {
    private userRepository;
    constructor(userRepository: Repository<User>);
    getAll(): Promise<User[]>;
    getById(id: number): Promise<User>;
    getByUserEmail(email: string): Promise<User | undefined>;
    getByUserEmailCheck(email: string): Promise<Object | boolean>;
    createUser(createUserDto: CreateUserDto): Promise<User>;
    deleteUserById(id: number): Promise<string>;
}
