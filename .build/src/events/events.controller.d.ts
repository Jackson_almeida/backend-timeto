import { CreateEventDto } from './create-event.dto';
import { Events } from './events.entity';
import { EventsService } from './events.service';
export declare class EventsController {
    private readonly eventsService;
    constructor(eventsService: EventsService);
    getAll(user: any): Promise<Events[]>;
    getById(id: number): Promise<Events>;
    create(user: any, createEventDto: CreateEventDto): Promise<Events>;
    editEvent(id: number, createEventDto: CreateEventDto): Promise<Events>;
    deleteEvent(id: number): Promise<any>;
}
