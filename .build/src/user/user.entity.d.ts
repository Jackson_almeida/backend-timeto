import { Events } from '../events/events.entity';
import { Invite } from '../invite/invite.entity';
export declare class User {
    id: number;
    username: string;
    email: string;
    password: string;
    events: Events[];
    invites: Invite[];
}
