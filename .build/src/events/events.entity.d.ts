import { Invite } from '../invite/invite.entity';
import { User } from '../user/user.entity';
export declare class Events {
    id: number;
    name: string;
    description: string;
    initialDatetime: Date;
    finalDatetime: Date;
    invites: Invite[];
    user: User;
}
