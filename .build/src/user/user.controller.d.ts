import { AuthService } from '../auth/auth.service';
import { CreateUserDto } from '../dto/create-user.dto';
import { User } from './user.entity';
import { UserService } from './user.service';
export declare class UserController {
    private readonly userService;
    private authService;
    constructor(userService: UserService, authService: AuthService);
    getAll(): Promise<User[]>;
    getById(id: number): Promise<User>;
    getByEmail(email: string): Promise<Object>;
    create(createUserDto: CreateUserDto): Promise<User>;
    delete(id: number): Promise<string>;
    login(req: any): Promise<{
        access_token: string;
    }>;
}
