import { InviteDto } from './invite.dto';
import { Invite } from './invite.entity';
import { InviteService } from './invite.service';
export declare class InviteController {
    private inviteService;
    constructor(inviteService: InviteService);
    findAll(user: any): Promise<Invite[]>;
    create(inviteDto: InviteDto): Promise<String | Invite>;
    acceptInvite(id: number): Promise<any>;
    rejectInvitation(id: number): Promise<any>;
    delete(id: number): Promise<any>;
}
