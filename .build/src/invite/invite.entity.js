"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Invite = exports.InviteStatus = void 0;
const events_entity_1 = require("../events/events.entity");
const user_entity_1 = require("../user/user.entity");
const typeorm_1 = require("typeorm");
var InviteStatus;
(function (InviteStatus) {
    InviteStatus["ACCEPTED"] = "accepted";
    InviteStatus["REJECTED"] = "rejected";
    InviteStatus["PENDING"] = "pending";
})(InviteStatus = exports.InviteStatus || (exports.InviteStatus = {}));
let Invite = class Invite {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], Invite.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => user_entity_1.User, user => user.invites, { eager: true }),
    __metadata("design:type", user_entity_1.User)
], Invite.prototype, "guestUser", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => events_entity_1.Events, event => event.invites, { eager: true }),
    __metadata("design:type", events_entity_1.Events)
], Invite.prototype, "event", void 0);
__decorate([
    (0, typeorm_1.Column)({
        type: 'enum',
        enum: InviteStatus,
        default: InviteStatus.PENDING
    }),
    __metadata("design:type", String)
], Invite.prototype, "status", void 0);
Invite = __decorate([
    (0, typeorm_1.Entity)()
], Invite);
exports.Invite = Invite;
//# sourceMappingURL=invite.entity.js.map