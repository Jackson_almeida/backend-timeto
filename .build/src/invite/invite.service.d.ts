import { Repository } from 'typeorm';
import { EventsService } from '../events/events.service';
import { UserService } from '../user/user.service';
import { InviteDto } from './invite.dto';
import { Invite } from './invite.entity';
export declare class InviteService {
    private inviteRepository;
    private eventService;
    private userService;
    constructor(inviteRepository: Repository<Invite>, eventService: EventsService, userService: UserService);
    getAllByUser(id: number): Promise<Invite[]>;
    createInvite(inviteDto: InviteDto): Promise<Invite | String>;
    acceptInvite(id: number): Promise<any>;
    rejectInvite(id: number): Promise<any>;
    deleteInvite(id: number): Promise<any>;
}
