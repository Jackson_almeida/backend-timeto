import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { EventsModule } from './events/events.module';
import { AuthModule } from './auth/auth.module';
import { APP_GUARD } from '@nestjs/core';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { InviteModule } from './invite/invite.module';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    UserModule,
    EventsModule,
    AuthModule,
    InviteModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host:  process.env.DB_HOST,
      port: 3306,
      username: 'jackson',
      password: '!123ABCabc',
      database: 'timeto',
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      synchronize: false, // LEMBRETE: Não usar como TRUE em uma eventual produção, RISCO DE PERDA DE DADOS!
    }),
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard, // proteje todas a rotas
    },
  ],
})
export class AppModule {}
