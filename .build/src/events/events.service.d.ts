import { UserService } from '../user/user.service';
import { Repository } from 'typeorm';
import { CreateEventDto } from './create-event.dto';
import { Events } from './events.entity';
export declare class EventsService {
    private eventsRepository;
    private userService;
    constructor(eventsRepository: Repository<Events>, userService: UserService);
    getAll(): Promise<Events[]>;
    getById(id: number): Promise<Events>;
    getAllByUser(id: number): Promise<Events[]>;
    createEvent(userId: number, createEventDto: CreateEventDto): Promise<Events>;
    editEvent(id: number, createEventDto: CreateEventDto): Promise<Events>;
    deleteEvent(id: number): Promise<any>;
}
